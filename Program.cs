﻿using System;

// Define the Strategy interface
public interface IFruitStrategy
{
    void Eat();
}

// Concrete strategy classes
public class AppleStrategy : IFruitStrategy
{
    public void Eat()
    {
        Console.WriteLine("Eating an apple.");
    }
}

public class BananaStrategy : IFruitStrategy
{
    public void Eat()
    {
        Console.WriteLine("Eating a banana.");
    }
}

public class OrangeStrategy : IFruitStrategy
{
    public void Eat()
    {
        Console.WriteLine("Eating an orange.");
    }
}

// Context class that uses the strategy
public class FruitContext
{
    private IFruitStrategy _strategy;

    public FruitContext(IFruitStrategy strategy)
    {
        _strategy = strategy;
    }

    public void EatFruit()
    {
        _strategy.Eat();
    }
}

class Program
{
    static void Main()
    {
        // Create instances of different fruit strategies
        var appleStrategy = new AppleStrategy();
        var bananaStrategy = new BananaStrategy();
        var orangeStrategy = new OrangeStrategy();

        // Create context objects for different fruits
        var appleContext = new FruitContext(appleStrategy);
        var bananaContext = new FruitContext(bananaStrategy);
        var orangeContext = new FruitContext(orangeStrategy);

        // Use the context objects to eat fruits
        appleContext.EatFruit(); // Output: Eating an apple.
        bananaContext.EatFruit(); // Output: Eating a banana.
        orangeContext.EatFruit(); // Output: Eating an orange.
    }
}
